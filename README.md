
### Accès à la **[documentation en ligne](https://nathalie.rousse.pages.mia.inra.fr/pydynamics) du paquet pydynamics**

# Programmer sur la plate-forme RECORD (http://www.inra.fr/record) son modèle en langage Python

Le paquet VLE pydynamics permet à chacun de programmer en langage Python ses modèles VLE (https://www.vle-project.org) (correspondant à la classe vle::devs::Dynamics).  

Le code source du paquet pydynamics se trouve dans le dépôt de sources Git du projet 'pydynamics' hébergé sous la forge forgemia https://forgemia.inra.fr/record/pydynamics.

### Principe d'utilisation du paquet pydynamics pour programmer son modèle en Python :

- Préalable : installer le paquet pydynamics.

- Création de son modèle VLE en langage Python :

    - créer son propre paquet VLE mon_paquet_vle.

    - créer (dans le répertoire src du paquet mon_paquet_vle) un fichier Python MonModelePython.py pour y programmer le modèle Python, qui se présente sous la forme d'une classe MonModelePython héritant de la classe PyDynamics.Dynamics.

    - créer un fichier vpz mon.vpz contenant un modèle atomique mon_modele_python pour le modèle Python. Le modèle atomique mon_modele_python a :

      - comme 'dynamic' la dynamique correspondant au modèle PyDynamics du fichier PyDynamics.cpp du paquet pydynamics.

      - comme paramètres dans ses conditions : **pythonpkgname**=mon_paquet_vle et **pythonmodelname**=MonModelePython.

- *Pour plus de détails et un exemple (MyModelPy.py, my_model_py.vpz...) : voir la documentation et le code du paquet pydynamics.*

### Mise en ligne de la documentation du paquet pydynamics :

Copier le répertoire pydynamics (qui contient la page de documentation index.html).

**CI/CD** : La documentation du paquet pydynamics est publiée sous : **[Online Documentation](https://nathalie.rousse.pages.mia.inra.fr/pydynamics)**.

*Autre moyen d'accès* : **[View pydynamics/index.html](https://htmlpreview.github.io/?https://forgemia.inra.fr/nathalie.rousse/pydynamics/-/raw/master/pydynamics/index.html)** *(method  using [GitHub & BitBucket HTML Preview](https://htmlpreview.github.io/))*

